<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->dateTime('delivery_date');
            $table->enum('schedule_init',['08:00', 
                                          '09:00', 
                                          '10:00', 
                                          '11:00', 
                                          '12:00', 
                                          '13:00', 
                                          '14:00', 
                                          '15:00', 
                                          '16:00', 
                                          '17:00', 
                                          '18:00']);
            $table->enum('schedule_time',[1,2,3,4,5,6,7,8]);
            $table->integer('driver_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
