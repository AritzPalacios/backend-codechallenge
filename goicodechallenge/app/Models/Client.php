<?php
/**
 * Description of Order
 *
 * @author aritz
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';
    
    /**
     * One to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function addresses() 
    {
      return $this->hasMany('App\Models\Address');
    }
    
    /**
     * One to Many relation
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function orders() 
    {
      return $this->hasMany('App\Models\Order');
    }

}