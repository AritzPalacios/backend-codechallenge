<?php
/**
 * Description of Order
 *
 * @author aritz
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';
    
    /**
    * One to Many relation
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function client() 
   {
        return $this->belongsTo('App\Models\Client');
   }
}