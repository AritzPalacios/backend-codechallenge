<?php
/**
 * Description of Order
 *
 * @author aritz
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';
    
     public function address()
    {
        return $this->hasOne('App\Address');
    }
}