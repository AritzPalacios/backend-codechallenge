<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Driver;

class OrderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $driver = Driver::inRandomOrder()->first();
        $order = new Order;
        $order->client_id = $request->client_id;
        $order->address_id = $request->address_id;
        $order->delivery_date = $request->delivery_date;
        $order->schedule_init = $request->schedule_init;
        $order->schedule_time = $request->schedule_time;
        $order->driver_id = $driver->id;
        $order->save();
    }
}
