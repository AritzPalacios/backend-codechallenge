<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($driverId, $date)
    {
        $orders = Order::where('driver_id', $driverId)
               ->where('delivery_date', $date)
               ->orderBy('schedule_init', 'asc')
               ->get();
        
        return json_encode($orders);
    }
}
